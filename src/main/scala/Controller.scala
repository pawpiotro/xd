import TextAnalyzing._
import TextLoading._

class Controller(var textLoader: TextLoader = null, val textAnalyzer: TextAnalyzer = new TextAnalyzer) {

  def setLoader(path:String, maxChunkSize:Int): Unit ={
      textLoader = new TextLoader(path, maxChunkSize)

  }

  def process(): Unit ={
    val containerClassName = "pam _3-95 _2pi0 _2lej uiBoxWhite noborder"
    val personClassName = "_3-96 _2pio _2lek _2lel"
    val messageClassName = "_3-96 _2let"
    val dataClassName = "_3-94 _2lem"
    textAnalyzer.analyze(textLoader.accessFile, containerClassName,personClassName, messageClassName)
  }
}
