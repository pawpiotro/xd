import java.text.ParseException

object Main {
  def main(args: Array[String]) = {

    val test = new Test
    test.xd


    if (args.size < 2) {
      println("Not enough arguments\n")
      System.exit(1)
    }


    try{
      val controller = new Controller
      controller.setLoader(args(0), args(1).toInt)

      controller.process()
    } catch {
      case e: ParseException => {
        println("Second argument needs to be number")
        System.exit(1)
      }
      case e: Exception => {
        e.printStackTrace()
        System.exit(1)
      }
    }
  }
}