package TextLoading

import java.io.{File, FileNotFoundException, IOException}

import org.jsoup.Jsoup
import org.jsoup.nodes.Document


class TextLoader(val path: String, var maxChunkSize: Int){
  private var fileSize = 0
  private var loadedBytes = 0

  def changeMaxSize(size: Int): Unit ={
    maxChunkSize = size
  }

  def accessFile: Document ={
    try{
      val file:File  = new File(path)
      if(!file.canRead()){
        println("Couldn't read file.\n")
        System.exit(1)
      }
      //for (line <- bufferedSource.getLines) {
      //  println(line.toUpperCase)
      //}
      val doc:Document = Jsoup.parse(file, "UTF-8")
      return doc
    } catch {
      case e:
        FileNotFoundException => println("Couldn't find that file.")
        return null
      case e:
        IOException => println("Got an IOException!\n" + e.printStackTrace())
        return null
    }
  }

  def loadChunk: String = {
    return null
  }
}
