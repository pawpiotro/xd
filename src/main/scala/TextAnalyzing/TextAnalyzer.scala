package TextAnalyzing

import java.text.NumberFormat

import org.jsoup.nodes.{Document, Element}

import scala.collection.JavaConverters._


class TextAnalyzer{

  var persons = scala.collection.mutable.Map[String, Person]()

  def isXD(s: String): Boolean ={
    if(s.length < 2)
      return false
    if(s.contains("xd") || s.contains("Xd") || s.contains("XD") || s.contains("xD"))
      return true
    else
      return false
  }

  def analyze(document: Document, containerClassName:String, personClassName: String, messageClassName:String): Unit ={
    //print(document)
    val list = document.getElementsByClass(containerClassName).asScala
    list.map {
      element => {
        val children = element.children().asScala
        var name: String = null
        var msg: String = null
        children.map {
          child =>{
            if(child.className() == personClassName){
                name = child.text
              if(!persons.contains(child.text)){
                persons += (child.text -> new Person(child.text))
              }
            }

            if(child.className() == messageClassName){
              msg = child.text
            }

          }
        }
        if(name != null && msg != null)
          persons(name).addMsg(msg)
      }
    }

    for((k,v) <- persons){
      println(v.name)
      for(m <- v.messages){
        val tmp:Array[String] = m.split(" ")
        for(el <- tmp){
          v.wordsInc
          if(isXD(el))
            v.xdsInc
        }
        //println(" "+m)
      }
      println(" words: "+v.words)
      println("   xds: "+v.xds)
      val xdspercentage = v.xds.toFloat / v.words
      printf("  pxds: %.10f%%\n",xdspercentage)
    }

  }

  def analyze2(document: Document, containerClassName:String, personClassName: String, messageClassName:String): Unit ={
    //print(document)
    val list = document.getElementsByClass(containerClassName).asScala
    list.map {
      element => {
        val children = element.children().asScala
        children.map {
          child => {
            if (child.className() == messageClassName) {
              println(child.text)
            }
          }
        }
      }
    }
  }


}
