package TextAnalyzing

import scala.collection.mutable.ListBuffer

class Person(val name: String) {
  var msgNumber = 0
  var words = 0
  var emojis = 0
  var xds = 0
  var messages:ListBuffer[String] = new ListBuffer[String]

  def addMsg(msg: String): Unit ={
    messages.prepend(msg)
  }

  def wordsInc{
    words +=1
  }
  def msgNumberInc{
    msgNumber +=1
  }
  def emojisInc{
    emojis +=1
  }
  def xdsInc{
    xds +=1
  }

}
